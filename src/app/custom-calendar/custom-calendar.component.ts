import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-calendar',
  templateUrl: './custom-calendar.component.html',
  styleUrls: ['./custom-calendar.component.css']
})
export class CustomCalendarComponent implements OnInit {

  value: Date;

  constructor() { }

  ngOnInit() {
  }

  onSelectMethod(event: Date) {
    this.value = event;
    console.log('DAY SELECTED: ' + this.value);
  }

}
