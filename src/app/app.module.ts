import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {CalendarModule, CheckboxModule, DialogModule, ScheduleModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CustomCalendarComponent } from './custom-calendar/custom-calendar.component';
import { CustomScheduleComponent } from './custom-schedule/custom-schedule.component';
import {HttpModule} from '@angular/http';





@NgModule({
  declarations: [
    AppComponent,
    CustomCalendarComponent,
    CustomScheduleComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    CalendarModule,
    ScheduleModule,
    DialogModule,
    HttpModule,
    CheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}

/*

export class MyModel {
  value: Date;
}

  */
