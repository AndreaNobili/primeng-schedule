import { PrimengcliPage } from './app.po';

describe('primengcli App', () => {
  let page: PrimengcliPage;

  beforeEach(() => {
    page = new PrimengcliPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
